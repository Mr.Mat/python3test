# Python3Test

The purpose of this project is to verify if a code wrote in python 2.7 is available in python 3

When a file is pushed the CI pipeline is triggered and create a job which :

    * Build a python 3.7 container
    * Launch the bash command "python --version" in order to test if python 3.7 is working
    * Launch one job which execute this command "python *.py"
