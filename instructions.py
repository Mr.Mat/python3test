import os

#this function detect all python files of the current folder and interpret it in python 3
def files_names(path):
    
    #For every files in the current folder
    for file in os.listdir(path):
        #if the file's name contains ".py"
        if  file.find(".py") != -1:
            #if the file's name is not "instructions.py" 
            if file != "instructions.py":
                #we lanch the linux command
                command = "python " + path + '/' + file 
                os.system(command) 
                

#this functon detect all the folders of the current folder and put them in a list
#this function return a list with all the folders contained in the current folder 
def detect_folder(path):

    # folder list which will contain the folder
    list_dir = []
    
    #for each file in the folder
    for folder in os.listdir(path):
        #if the file is a folder we add it to our list
        if os.path.isdir(os.path.join(path,folder)):
            list_dir.append(os.path.join(path,folder))
    return list_dir
    
    
def instruction(path):
    #if path is None
    if path == None:
        return None
    
    #if path is empty
    if path == '':
        return None

    #else we interpret the python files of the current folder
    files_names(path)
    #and we get the folders of the current folder
    list_folder = detect_folder(path)

    #we do the same thing for each folders
    for folder in list_folder:
        new_path = folder 
        instruction(folder)


instruction('.')
