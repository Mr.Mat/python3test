pipeline {
    agent any

    stages {
        stage('Build') {
            steps {
                step([$class: 'Build Timestamp']){
                    echo 'Building oh yeah ... ${BUILD_TIMESTAMP}'
                }
            }
        }
        stage('Test') {
            steps {
                echo 'Testing..'
            }
        }
        stage('Deploy') {
            steps {
                echo 'Deploying....'
            }
        }
    }
}
